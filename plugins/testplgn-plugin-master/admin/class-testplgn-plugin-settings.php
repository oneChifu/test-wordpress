<?php

/**
 * The settings of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Testplgn_Plugin
 * @subpackage Testplgn_Plugin/admin
 */

/**
 * Class WordPress_Plugin_Template_Settings
 *
 */
class Testplgn_Admin_Settings {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * This function introduces the theme options into the 'Appearance' menu and into a top-level
	 * 'Testplgn' menu.
	 */
	public function setup_plugin_options_menu() {

		//Add the menu to the Plugins set of menu items
		add_menu_page( 
			'Test Plugin', 
			'Test Plugin', 
			'manage_options', 
			'Testplgn_options', 
			array( $this, 'render_settings_page_content'), 
			'', 
			81
		); 
	}

	/**
	 * Provides default values for the Input Options.
	 *
	 * @return array
	 */
	public function default_input_options() {

		$defaults = array(
			'textarea_example'	=>	'Hello World!',
		);

		return $defaults;

	}

	/**
	 * Renders a simple page to display for the theme menu defined above.
	 */
	public function render_settings_page_content( $active_tab = '' ) {
		?>
		<!-- Create a header in the default WordPress 'wrap' container -->
		<div class="wrap">

			<h2>TestPLG display content</h2>
			<?php settings_errors(); ?>

			<form method="post" action="options.php">
				<?php
				settings_fields( 'Testplgn_input_examples' );
				do_settings_sections( 'Testplgn_input_examples' );

				submit_button();
				?>
			</form>

		</div><!-- /.wrap -->
	<?php
	}

	/**
	 * This function provides a simple description for the Input Examples page.
	 *
	 * It's called from the 'testplgn_theme_initialize_input_examples_options' function by being passed as a parameter
	 * in the add_settings_section function.
	 */
	public function input_examples_callback() {
		$options = get_option('Testplgn_input_examples');
		// var_dump($options);
		// echo '<p>' . __( 'Provides examples of the five basic element types.', 'testplgn-plugin' ) . '</p>';
	} // end general_options_callback


	/**
	 * Initializes the theme's input example by registering the Sections,
	 * Fields, and Settings. This particular group of options is used to demonstration
	 * validation and sanitization.
	 *
	 * This function is registered with the 'admin_init' hook.
	 */
	public function initialize_input_examples() {
		//delete_option('Testplgn_input_examples');
		if( false == get_option( 'Testplgn_input_examples' ) ) {
			$default_array = $this->default_input_options();
			update_option( 'Testplgn_input_examples', $default_array );
		} // end if

		add_settings_section(
			'input_examples_section',
			'This text will be display in post content',
			array( $this, 'input_examples_callback'),
			'Testplgn_input_examples'
		);

		add_settings_field(
			'Textarea Element',
			'Your text',
			array( $this, 'textarea_element_callback'),
			'Testplgn_input_examples',
			'input_examples_section'
		);

		register_setting(
			'Testplgn_input_examples',
			'Testplgn_input_examples',
			array( $this, 'validate_input_examples')
		);

	}

	public function textarea_element_callback() {

		$options = get_option( 'Testplgn_input_examples' );

		// Render the output
		echo '<textarea id="textarea_example" name="Testplgn_input_examples[textarea_example]" rows="5" cols="50">' . $options['textarea_example'] . '</textarea>';

	} // end textarea_element_callback

	public function validate_input_examples( $input ) {

		// Create our array for storing the validated options
		$output = array();

		// Loop through each of the incoming options
		foreach( $input as $key => $value ) {

			// Check to see if the current option has a value. If so, process it.
			if( isset( $input[$key] ) ) {

				// Strip all HTML and PHP tags and properly handle quoted strings
				$output[$key] = strip_tags( stripslashes( $input[ $key ] ) );

			} // end if

		} // end foreach

		// Return the array processing any additional functions filtered by this action
		return apply_filters( 'validate_input_examples', $output, $input );

	} // end validate_input_examples




}