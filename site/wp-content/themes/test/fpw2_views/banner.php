<?php
/**
 * "Banner Image" Layout Template File
 * 
 * DO NOT MODIFY THIS FILE!
 * 
 * To override, copy the /fpw2_views/ folder to your active theme's folder.
 * Modify the file in the theme's folder and the plugin will use it.
 * See: http://wordpress.org/extend/plugins/feature-a-page-widget/faq/
 * 
 * Note: Feature a Page Widget provides a variety of filters and options that may alter the output of the_title, the_excerpt, and the_post_thumbnail in this template.
 */
?>


<div class="article_block">
	<a href="<?php the_permalink(); ?>" class="article_block__link"><h2>О нас</h2></a>

	<div class="article_block__content">
		<?php the_excerpt(); ?>
	</div>
</div>
