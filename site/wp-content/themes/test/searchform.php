<div class="header__search">
	<form method="get" action="<?php echo esc_url( home_url() ); ?>">
		<input type="search" name="s" class="header__search__input" placeholder="Поиск">
	</form>
</div>
