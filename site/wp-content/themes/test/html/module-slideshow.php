<?php
$the_query = new WP_Query( array(
	// 'post_type' => 'news',
	'posts_per_page' => 10,
	'orderby' => 'post_date',
	'order' => 'DESC'
));

if ( $the_query->have_posts() ) : ?>

<div class="main_slideshow">
	<div class="container">
		<div class="swiper-container js-slider">
		    <div class="swiper-wrapper">
		    	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<div class="swiper-slide">
					<div class="row main_slideshow__item">
						<div class="col col-12 col-md-6 col-lg-4">
							<div class="main_slideshow__item__content">
								<a href="<?php echo esc_url( get_permalink() ); ?>" class="main_slideshow__item__title"><?php the_title(); ?></a>

								<div class="main_slideshow__item__info">
									<div class="main_slideshow__item__date"><i class="far fa-clock"></i><span><?php echo date_i18n(get_the_date('F j, Y')); ?></span></div>

									<?php foreach ( get_the_category() as $key => $value ) : ?>
										<a href="#" class="main_slideshow__item__category"><?php echo $value->name; ?></a>
									<?php endforeach; ?>
								</div>
							</div>
						</div>

						<div class="col col-12 col-md-6 col-lg-8">
							<div class="main_slideshow__item__image cover"><?php the_post_thumbnail(); ?></div>
						</div>
					</div>
				</div>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</div>

			<div class="swiper-navigation">
				<div class="swiper-prev"><i class="fas fa-caret-left"></i></div>
				<div class="swiper-next"><i class="fas fa-caret-right"></i></div>
			</div>
		</div>
	</div>
</div>

<?php endif; ?>