$(document).ready(function(){

//////////
// Global variables
//////////
var _window = $(window);
var _document = $(document);
var MobileMax = 767;
var Tablet = 768;
var TabletMax = 991;
var Desktop = 992;
var DesktopMax = 1199;
var Wide = 1200;
var WideMax = 1499;
var Hd = 1500;


////////////
// READY - triggered when PJAX DONE
////////////
function pageReady(){
	initSliders();
}

// this is a master function which should have all functionality
pageReady();


// some plugins work best with onload triggers
_window.on('load', function() {
bgImage('.cover, .contain');
})


// Prevent # behavior
_document
.on('click', '[href="#"]', function(e) {
		e.preventDefault();
	})
.on('click', 'a[href^="#section"]', function() { // section scroll
  var el = $(this).attr('href');
  $('body, html').animate({
      scrollTop: $(el).offset().top}, 1000);
  return false;
})



//////////
// SLIDERS
//////////
function initSliders(){
	var SwiperOptions = {
		wrapperClass: "swiper-wrapper",
		slideClass: "swiper-slide",
		direction: 'horizontal',
		loop: false,
		watchOverflow: true,
		setWrapperSize: false,
		spaceBetween: 0,
		slidesPerView: 1,
		autoHeight: true,
		// loop: true,
		normalizeSlideIndex: true,
		// centeredSlides: true,
		// freeMode: true,
		// effect: 'fade',
		autoplay: {
			delay: 3000
		},
		navigation: {
			nextEl: '.swiper-next',
			prevEl: '.swiper-prev'
		},
		pagination: {
			el: '.swiper-pagination',
			type: 'bullets',
			clickable: true
		}
		// breakpoints: {
		//   // when window width is <= 992px
		//   992: {
		//     autoHeight: true
		//   }
		// }
	};

	// EXAMPLE SWIPER
	new Swiper('.js-slider', SwiperOptions);
}

});



/**************************************************************************
Background images width lazy load
/**************************************************************************/
function bgImage(el) {
	$(el).each(function() {
		$el = $(this);

		if ( $el.is('.cover') || $el.is('.contain') ) {
			$el.css({
				backgroundImage: function() {
					return 'url('+$(this).find('>img').attr('src')+')';
				},
				backgroundRepeat: 'no-repeat',
				backgroundPosition: 'center',
				backgroundSize: function() {
					Value = '';
					if ( $el.is('.cover') ) {
						Value = 'cover';
					} else if ( $el.is('.contain') ) {
						Value = 'contain';
					}
					return Value;
				}
			}).addClass('is-loaded').find('>img').hide();
		}
	})
}
/**************************************************************************
END Background images width lazy load
/**************************************************************************/


/**************************************************************************
Header mobile menu
/**************************************************************************/
$(document).on('click', '.header__btn-menu', function(e) {
	e.preventDefault();
	if ( Modernizr.mq('(max-width: 767px)') ) {
		$('.header__menu_container').toggleClass('is-active');
		$('body').toggleClass('is-overflow');
	}
});
/**************************************************************************
END Header mobile menu
/**************************************************************************/

