<?php
$the_query = new WP_Query( array(
	'post_type' => 'news',
	'posts_per_page' => 3,
	'orderby' => 'post_date',
	'order' => 'DESC'
));

if ( $the_query->have_posts() ) : ?>
	
<div class="news">
	<h2>Новости</h2>

	<div class="news__list">
    	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<a href="<?php echo esc_url( get_permalink() ); ?>" class="news__item">
				<div class="news__item__image_container">
					<div class="news__item__image cover">
						<?php if ( has_post_thumbnail() ) : ?>
							<?php the_post_thumbnail(); ?>
						<?php endif; ?>
					</div>
				</div>

				<div class="news__item__date"><?php echo date_i18n(get_the_date('F j, Y')); ?></div>
				<div class="news__item__title"><?php the_title(); ?></div>
			</a>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
    </div>
</div>

<?php endif; ?>
