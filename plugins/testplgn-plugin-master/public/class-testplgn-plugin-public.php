<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://devinvinson.com
 * @since      1.0.0
 *
 * @package    Testplgn_Plugin
 * @subpackage Testplgn_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Testplgn_Plugin
 * @subpackage Testplgn_Plugin/public
 * @author     Devin Vinson <devinvinson@gmail.com>
 */
class Testplgn_Plugin_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Show content on fronted in post content
	 *
	 * @since    1.0.0
	 */
	public function set_content($content) {

		$options = get_option( 'Testplgn_input_examples' );

		if ( !empty($options['textarea_example']) ) {
			$content = sprintf(
	            '<div>'. $options['textarea_example'] .'</div><br>',
	            get_bloginfo( 'stylesheet_directory' ),
	            $content
	        );
		}

		return $content;

	}

}
