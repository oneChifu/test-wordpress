<?php

/**
 * Fired during plugin deactivation
 *
 * @since      1.0.0
 *
 * @package    Testplgn_Plugin
 * @subpackage Testplgn_Plugin/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Testplgn_Plugin
 * @subpackage Testplgn_Plugin/includes
 * @author     Devin Vinson <devinvinson@gmail.com>
 */
class Testplgn_Plugin_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
