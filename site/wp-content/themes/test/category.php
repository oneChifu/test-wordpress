<?php get_header(); ?>


<div class="body">
	<div class="container">
		<main class="content" role="main">
		<!-- section -->
		<section>

			<h1><?php esc_html_e( 'Category: ', 'html5blank' ); single_cat_title(); ?></h1>

			<?php get_template_part( 'loop' ); ?>

			<?php get_template_part( 'pagination' ); ?>

		</section>
		<!-- /section -->
		</main>
	</div>
</div>


<?php get_footer(); ?>
