<!doctype html>
<html <?php language_attributes(); ?>>

<head>
<meta charset="<?php bloginfo('charset'); ?>">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/img/logo.png" />
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png" />
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicon-16x16.png">
<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />

<title><?php echo wp_get_document_title(); ?></title>
<meta name="keywords" content="<?php bloginfo('keywords'); ?>">
<meta name="description" content="<?php bloginfo('description'); ?>" />

<meta itemprop="name" content="<?php echo ( get_the_title() && !is_front_page() ) ? get_the_title() : wp_get_document_title(); ?>">
<meta itemprop="description" content="<?php echo ( has_excerpt() ) ? get_the_excerpt() : bloginfo('description'); ?>">
<meta itemprop="image" content="<?php echo ( has_post_thumbnail() ) ? get_the_post_thumbnail_url() : get_template_directory_uri() .'/img/logo.png'; ?>">

<meta property="og:title" content="<?php echo ( get_the_title() && !is_front_page() ) ? get_the_title() : wp_get_document_title(); ?>">
<meta property="og:type" content="<?php echo ( is_front_page() ) ? 'site' : 'article'; ?>">
<meta property="og:url" content="<?php echo the_permalink(); ?>">
<meta property="og:image" content="<?php echo ( has_post_thumbnail() && !is_front_page() ) ? get_the_post_thumbnail_url() : get_template_directory_uri() .'/img/logo.png'; ?>">
<meta property="og:description" content="<?php echo ( has_excerpt() ) ? get_the_excerpt() : bloginfo('description'); ?>">
<meta property="og:site_name" content="<?php bloginfo('name'); ?>">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<?php wp_head(); ?>
</head>

<body role="page" <?php body_class(); ?>>
<div id="wrapper">
<header class="header">
	<div class="header__top">
		<div class="container">
			<div class="row">
				<div class="col col-12 col-sm-6">
					<div class="header__btn-menu"><i class="fas fa-bars"></i></div>

					<a href="<?php echo home_url(); ?>" class="header__logo"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png"></a>
				</div>

				<div class="col col-12 col-sm-6">
					<div class="header__phone"><a href="tel:+3806898990">+380 689 89 90</a></div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="header__bottom">
		<div class="container">
			<div class="row">
				<div class="col col-12 col-lg-8">
					<div class="header__menu_container">
						<div class="container">
							<div class="header__btn-menu"><i class="fas fa-times"></i></div>

							<nav class="header__menu" role="navigation">
								<?php header_nav(); ?>
							</nav>
						</div>
					</div>
				</div>
				
				<div class="col col-12 col-lg-4">
					<?php if ( is_active_sidebar( 'widget-header' ) ) : ?>
						<?php dynamic_sidebar('widget-header'); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</header>


