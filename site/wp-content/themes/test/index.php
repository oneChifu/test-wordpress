<?php get_header(); ?>


<?php get_template_part( 'html/module', 'slideshow' ); ?>


<div class="body">
	<div class="container">
		<div class="row">
			<div class="col col-12 col-lg-4">
				<?php if ( is_active_sidebar( 'widget-leftside' ) ) : ?>
					<?php dynamic_sidebar('widget-leftside'); ?>
				<?php endif; ?>
			</div>


			<div class="col col-12 col-lg-8">
				<main class="content" role="main">
					<?php get_template_part( 'html/module', 'news' ); ?>
				</main>
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>
